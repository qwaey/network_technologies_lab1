import net.ccfit.kitaev.network.Countable
import net.ccfit.kitaev.network.Receiver
import net.ccfit.kitaev.network.Sender
import org.junit.Assert
import org.junit.Test

class StartTest(ip: String, port: Int) {
    private val sender = Sender(ip, port)
    private val receiver = Receiver(ip, port)

    fun start(): Countable {
        sender.start()
        return receiver.apply {
            this.start()
        }
    }

    fun interrupt() {
        sender.interrupt()
        receiver.interrupt()
    }
}

class MainTest {
    @Test
    fun singleTest() {
        val tester1 = StartTest("224.0.0.224", 2244)
        val checker1 = tester1.start()

        Thread.sleep(2000)
        Assert.assertEquals(checker1.getCount(), 1)

        val tester2 = StartTest("224.0.0.224", 2244)
        val checker2 = tester2.start()

        Thread.sleep(6000)
        Assert.assertEquals(checker1.getCount(), 2)
        Assert.assertEquals(checker2.getCount(), 2)
    }
}