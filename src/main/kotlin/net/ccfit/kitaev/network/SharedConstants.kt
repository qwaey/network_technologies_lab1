package net.ccfit.kitaev.network

/**
 * Copyright (c) 2018 Andrey Kitaev.
 */
class SharedConstants {

    companion object {
        const val BUFFER_SIZE = 1024
        const val DEFAULT_PORT = 2244
        const val DEFAULT_MULTICAST_GROUP = "224.0.0.224"
        const val DEFAULT_SEND_DELAY = 1000L
        const val DEFAULT_TIMEOUT = 16
    }
}