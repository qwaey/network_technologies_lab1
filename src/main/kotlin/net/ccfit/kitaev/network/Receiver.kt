package net.ccfit.kitaev.network

import java.net.DatagramPacket
import java.net.InetAddress
import java.net.MulticastSocket

/**
 * Copyright (c) 2018 Andrey Kitaev.
 */
class Receiver(
        private var ip: String,
        port: Int
) : Thread("Receiver"), Countable {

    private val address = InetAddress.getByName(ip)
    private val socket = MulticastSocket(port).apply {
        this.joinGroup(address)
    }

    private val messageHandler = MessageHandler().apply {
        this.start()
    }

    override fun getCount(): Int {
        return messageHandler.getCount()
    }

    override fun run() {
        if (!address.isMulticastAddress) {
            println("Address $ip is not multicast. The default address will be used")
            ip = SharedConstants.DEFAULT_MULTICAST_GROUP
        }

        while (Thread.currentThread().isAlive) {
            messageHandler.handle(getMultiCastMessage())
        }
    }

    private fun getMultiCastMessage(): Pair<String, String> {
        val buf = ByteArray(SharedConstants.BUFFER_SIZE)
        val packet = DatagramPacket(buf, buf.size)
        socket.receive(packet)
        return Pair(String(packet.data), packet.address.hostAddress)
    }
}