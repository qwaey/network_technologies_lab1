package net.ccfit.kitaev.network

import java.io.IOException
import java.net.DatagramPacket
import java.net.DatagramSocket
import java.net.InetAddress
import java.util.*

/**
 * Copyright (c) 2018 Andrey Kitaev.
 */
class Sender(private val ip: String, private val port: Int) : Thread("Sender") {
    private val socket = DatagramSocket()
    private val buffer = UUID.randomUUID().toString().toByteArray()

    override fun run() {
        val group = InetAddress.getByName(ip)
        val packet = DatagramPacket(buffer, buffer.size, group, port)

        while (Thread.currentThread().isAlive) {
            try {
                socket.send(packet)
                sleep(SharedConstants.DEFAULT_SEND_DELAY)
            } catch (e: InterruptedException) {
            } catch (e: IOException) {
                e.printStackTrace()
                socket.close()
                break
            }
        }
    }
}