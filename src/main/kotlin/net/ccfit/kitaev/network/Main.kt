package net.ccfit.kitaev.network

/**
 * Copyright (c) 2018 Andrey Kitaev.
 */
fun main(args: Array<String>) {
    val (ip, port) = IpParser.parse(args)
    Sender(ip, port).start()
    Receiver(ip, port).start()
}