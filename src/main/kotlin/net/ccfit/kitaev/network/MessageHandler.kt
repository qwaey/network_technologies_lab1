package net.ccfit.kitaev.network

import java.util.concurrent.ConcurrentHashMap

/**
 * Copyright (c) 2018 Andrey Kitaev.
 */
class MessageHandler : Thread("Collector"), Countable {

    private val storage = ConcurrentHashMap<String, ConnectionTimer>()
    private val SECOND = 1000L

    override fun getCount() = storage.size

    override fun run() {
        while (Thread.currentThread().isAlive) {
            val iterator = storage.iterator()

            while (iterator.hasNext()) {
                val entryPair = iterator.next().apply {
                    this.value.decrementTimeToLife()
                }

                if (!entryPair.value.isAlive()) {
                    iterator.remove()
                    printConnections()
                }
            }

            try {
                sleep(SECOND)
            } catch (e: InterruptedException) {
            }
        }
    }

    fun handle(messagePair: Pair<String, String>) {
        val (message, ip) = messagePair

        if (storage.containsKey(message)) {
            storage[message]?.update()
        } else {
            storage[message] = ConnectionTimer(ip, SharedConstants.DEFAULT_TIMEOUT)
            printConnections()
        }
    }

    private fun printConnections() {
        println("In the local network there are ${storage.size} copies of the program")
        storage.forEach {
            println("Ip: [${it.value.ip}] with UUID: [${it.key}]")
        }
    }
}