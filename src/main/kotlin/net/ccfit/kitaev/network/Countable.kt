package net.ccfit.kitaev.network

/**
 * Copyright (c) 2018 Andrey Kitaev.
 */
interface Countable {
    fun getCount(): Int
}