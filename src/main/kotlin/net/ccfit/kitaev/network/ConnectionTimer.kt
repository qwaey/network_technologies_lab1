package net.ccfit.kitaev.network

/**
 * Copyright (c) 2018 Andrey Kitaev.
 */
class ConnectionTimer(val ip: String, private val timeToLife: Int) {

    private var clock = timeToLife

    fun update() {
        clock = timeToLife
    }

    fun decrementTimeToLife() {
        --clock
    }

    fun isAlive(): Boolean {
        return (clock > 0)
    }
}