package net.ccfit.kitaev.network

/**
 * Copyright (c) 2018 Andrey Kitaev.
 */
class IpParser {

    companion object {
        fun parse(args: Array<String>): Pair<String, Int> {
            return when (args.size) {
                1 -> Pair(args[0], SharedConstants.DEFAULT_PORT)
                2 -> Pair(args[0], args[1].toInt())
                else -> Pair(SharedConstants.DEFAULT_MULTICAST_GROUP, SharedConstants.DEFAULT_PORT)
            }
        }
    }
}